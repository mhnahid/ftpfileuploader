import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


public class MyFile {
	private static void showServerReply(FTPClient ftpClient) {
		String [] replies= ftpClient.getReplyStrings();
		if(replies !=null && replies.length>0){
			for (String aReply : replies){
				System.out.println("SERVER: "+ aReply);
			}
		}
	}
	private static void disconnect( FTPClient ftpClient){
		try{
			if(ftpClient.isConnected()){
				ftpClient.logout();
				ftpClient.disconnect();
				System.out.println("Disconnected");
			}
			else{
				System.out.println(" All ready Disconnected");
			}
		}catch(IOException ex){
			ex.printStackTrace();

		}


	}
	private static boolean ServerConnectStatus( String server,int port, String username, String pass, FTPClient ftpClient){
		boolean flag=false;
		try{

			ftpClient.connect(server, port);
			showServerReply(ftpClient);
			int replyCode=ftpClient.getReplyCode();
			if(!FTPReply.isPositiveCompletion(replyCode)){
				System.out.println("Operation faild. Server Reply Code:"+replyCode);
				//return;
			}
			boolean success =ftpClient.login(username, pass);
			showServerReply(ftpClient);
			if(!success){
				System.out.println("Login Failed");
				flag=false;
			}else{
				System.out.println("Log in successfull");
				flag=true;
			}

		}catch(IOException ex){
			System.out.println("System failed");
			ex.printStackTrace();

		}
		return flag;
	}
	private static void  makeDir(FTPClient ftpClient, String dirPath) {
		try{
			String[] pathElements = dirPath.split("/");
			if (pathElements != null && pathElements.length > 0) {
				for (String singleDir : pathElements) {
					boolean existed = ftpClient.changeWorkingDirectory(singleDir);
					if (!existed) {
						boolean created = ftpClient.makeDirectory(singleDir);
						if (created) {
							System.out.println("CREATED directory: " + singleDir);
							ftpClient.changeWorkingDirectory(singleDir);
						} else {
							System.out.println("COULD NOT create directory: " + singleDir);
						}
					}
				}
			}

		}catch(IOException ex){
			System.out.println("System failed");
			ex.printStackTrace();
		}
	}

	private static  boolean fileUpload(FTPClient ftpClient, byte [] content, String remoteFile){
		boolean flag=false;
		try{
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			InputStream inputSteram = null;
			inputSteram = new ByteArrayInputStream(content);

			System.out.println("File uploading....");
			boolean result= ftpClient.storeFile(remoteFile, inputSteram);
			//inputStream.close();
			if(result){
				flag=true;
				System.out.println("File Uploaded Successfully");
			}else{
				System.out.println("File Uploading Failed");
			}

		}catch(IOException ex){
			ex.printStackTrace();
		}
		return flag;
	}

	public static void main(String[] args) {
		String server="ftp address";
		int port=21;
		String username="username";
		String pass="password";
		FTPClient ftpClient =new FTPClient();
		if(ServerConnectStatus(server, port, username, pass, ftpClient)==true){
			System.out.println("Connected");
			ftpClient.enterLocalPassiveMode();

			LocalDate date = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String text = date.format(formatter);

			String dirPath="/"+text;

			makeDir(ftpClient, dirPath);
			String remoteFile="log1.txt";
			String line="Hello world";
			byte [] s= line.getBytes();
			fileUpload(ftpClient,s,remoteFile);

			disconnect(ftpClient);
		}
	}

}
